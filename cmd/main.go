package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"sort"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"

	"muze.j7labs.de/pkg/config"
	"muze.j7labs.de/pkg/downloads"
	"muze.j7labs.de/pkg/frontend"
	"muze.j7labs.de/pkg/jobs"
	"muze.j7labs.de/pkg/messages"
	"muze.j7labs.de/pkg/persistence"
	"muze.j7labs.de/pkg/pubsub"
	"muze.j7labs.de/pkg/view"
	"muze.j7labs.de/pkg/wslink"
)

type Post struct {
	Url string `json:"url"`
}

type PostList struct {
	List []*Post `json:"List"`
}

type MessageType int

const (
	USERINFO MessageType = iota
	DOWNLOAD
	JOBUPDATE
	QUEUELENGTH
	LABELS
	LABEL
)

type Message struct {
	MessageType MessageType
	Message     interface{}
	Elapses     int
}

type JobInfoMessage struct {
	Name   string
	Status string
}

type UserInfoMessagedeleteme struct {
	Name   string
	Status string
}

type UserInfodeletemt struct {
	Messages []*Message
}

type selectedLables struct {
	Name   string
	Labels map[string]bool
}

type LoggingHandler struct {
	Handler  http.Handler
	logTopic *pubsub.Topic
}

var (
	dbConfig persistence.DatabaseConfig
)

func NewLoggingHandler(prefix string, folder string) *LoggingHandler {
	t, err := pubsub.GetTopicByName(messages.TopicUrl)
	if err != nil {
		log.Panic(err.Error())
	}
	return &LoggingHandler{
		Handler:  http.StripPrefix(prefix, http.FileServer(http.Dir(folder))),
		logTopic: t,
	}
}

func (l LoggingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	g := r.URL.Path

	decodedValue, err := url.QueryUnescape(g)
	if err != nil {
		log.Fatal(err)
	}
	l.logTopic.Publish(decodedValue)

	l.Handler.ServeHTTP(w, r)

}

var configData = config.Default()

type status struct {
	Label     string
	LabelType string
	Album     string
}

var mylinks chan messages.Post
var reloadlinks chan messages.ReloadPost

var templates *template.Template
var jobsManager *jobs.Jobs

var database *persistence.Database

// var messagetimer = time.NewTicker(2 * time.Second)
var jobticker = time.NewTicker(1 * time.Second)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func spa(w http.ResponseWriter, r *http.Request) {
	label := r.URL.Query().Get("label")
	titel := r.URL.Query().Get("titel")
	album := r.URL.Query().Get("album")
	fmt.Println(label, titel, album)

	st := status{
		Label:     "Baroque",
		LabelType: "CONTENT",
		Album:     album,
	}
	log.Println("spa template call ", st)
	err := templates.ExecuteTemplate(w, "spa.html", st)
	if err != nil {
		fmt.Println(err.Error())
	}
	if label != "" {
		t, _ := pubsub.GetTopicByName(messages.TopicClients)
		m := &messages.Message{MessageType: messages.LABELS, Message: "", Elapses: 4}
		s, _ := json.Marshal(m)
		t.Publish(string(s))
	}

	if titel != "x" {
		t, _ := pubsub.GetTopicByName(messages.TopicClients)
		m := &messages.Message{MessageType: messages.TITLE, Message: "", Elapses: 4}
		s, _ := json.Marshal(m)
		t.Publish(string(s))
	}
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
	v := view.ViewData{}
	err := templates.ExecuteTemplate(w, "index.html", v.Pieces)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func handleDrops(w http.ResponseWriter, r *http.Request) {

	u := messages.Post{}
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		fmt.Println(err)
		http.Error(w, "Error decoding response object", http.StatusBadRequest)
		return
	}
	fmt.Println("Found", len(mylinks))

	l, _ := pubsub.GetTopicByName(messages.TopicUrl)
	l.Publish((u.Url))

	mylinks <- u
	handleQueueLength(w, r)
}

func handleReload(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")

	p, _ := database.GetPiece(name)

	u := messages.ReloadPost{
		Url:   p.Link,
		Name:  p.Name,
		Title: p.Title,
	}

	l, _ := pubsub.GetTopicByName(messages.TopicUrl)
	l.Publish((u.Url))

	reloadlinks <- u
	handleQueueLength(w, r)
}

func handleAlbums(w http.ResponseWriter, r *http.Request) {

	label := r.URL.Query().Get("label")
	labeltype := r.URL.Query().Get("type")

	v := view.ViewData{}
	keys := make([]string, 0, len(database.PiecesMap))

	for k := range database.PiecesMap {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return database.PiecesMap[keys[i]].TimeAdded.After(database.PiecesMap[keys[j]].TimeAdded)
	})
	//sort.Strings(keys)
	//sort.Slice(timeSlice, func(i, j int) bool {
	//	return timeSlice[i].date.Before(timeSlice[j].date)
	//})

	if labeltype == "CONTROL" {
		switch label {
		case "NOLABELS":
			for _, k := range keys {
				p := database.PiecesMap[k]
				if len(p.Labels) == 0 {
					v.Pieces = append(v.Pieces, view.FromPV(p))
				}
			}
		case "":
			for _, k := range keys {
				p := database.PiecesMap[k]
				v.Pieces = append(v.Pieces, view.FromPV(p))
			}
		}

	} else {

		for _, k := range keys {
			p := database.PiecesMap[k]
			for _, l := range p.Labels {
				if l == label || label == "" {
					v.Pieces = append(v.Pieces, view.FromPV(p))
					break
				}
			}
		}
	}

	err := templates.ExecuteTemplate(w, "albums.html", v.Pieces)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func handleJobList(w http.ResponseWriter, r *http.Request) {
	err := json.NewEncoder(w).Encode(jobsManager.Jobs)
	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}
}

func handleQueueLength(w http.ResponseWriter, r *http.Request) {

	l := len(mylinks)
	err := json.NewEncoder(w).Encode(l)
	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}
}

func handleSetLabels(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")
	fmt.Println(name)
	l := make([]string, 0)
	if err := json.NewDecoder(r.Body).Decode(&l); err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}

	err := database.SetLabels(name, l, dbConfig)
	if err != nil {
		return
	}

	result := ""
	err = json.NewEncoder(w).Encode(result)
	if err != nil {
		return
	}

	t, _ := pubsub.GetTopicByName(messages.TopicClients)
	m := &messages.Message{MessageType: messages.LABELS, Message: "", Elapses: 4}
	s, _ := json.Marshal(m)
	t.Publish(string(s))
}

func handleGetLabels(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")
	fmt.Println(name)

	labels, err := database.GetSelectedLabelsForAlbum(name)
	if err != nil {
		fmt.Printf("Error retrieving album %v", err)
	}

	p := selectedLables{Name: name, Labels: labels}

	err = templates.ExecuteTemplate(w, "labeledit.html", p)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func handleLabels(w http.ResponseWriter, r *http.Request) {
	v := view.ViewData{}
	v.Labels = database.AllLabels

	err := templates.ExecuteTemplate(w, "labels.html", v)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func handlePieceInfo(w http.ResponseWriter, r *http.Request) {

	name := r.URL.Query().Get("name")
	p, _ := database.GetPiece(name)

	v := view.PieceInfoFromPV(p)
	err := templates.ExecuteTemplate(w, "pieceInfo.html", v)
	if err != nil {
		fmt.Println(err.Error())
	}
}

func startLogging() {

	t, err := pubsub.GetTopicByName(messages.TopicLogging)
	if err != nil {
		log.Panic(err.Error())
	}
	cl := t.Subscribe()

	t, err = pubsub.GetTopicByName(messages.TopicUrl)
	if err != nil {
		log.Panic(err.Error())
	}
	cu := t.Subscribe()

	f, err := os.OpenFile(configData.LogFile,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Panic(err.Error())
	}
	defer f.Close()

	for {
		select {
		case m := <-*cl:
			fmt.Println("Message:" + m)
			if _, err := f.WriteString(m + "\n"); err != nil {
				log.Println(err)
			}
		case m := <-*cu:
			fmt.Println("Url:" + m)
			if _, err := f.WriteString(m + "\n"); err != nil {
				log.Println(err)
			}

		}
	}
}

func echo(w http.ResponseWriter, r *http.Request) {
	log.Println("echo", r)
	log.Println("echo", r)
	c, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Print("upgrade:", err)
		return
	}

	t, _ := pubsub.GetTopicByName(messages.TopicClients)
	oc := t.Subscribe()
	_ = wslink.NewWSClient(*c, oc, nil)

}

func main() {

	configpath := flag.String("configfile", "", "the config file")
	flag.Parse()

	if *configpath != "" {
		configData = config.ReadConfig(*configpath)
	}
	spew.Dump(configData)

	fmt.Println("Mains")
	logrus.Info("main")
	logrus.SetLevel(logrus.DebugLevel)
	logrus.Info("topics")

	_ = pubsub.NewTopic(messages.TopicLogging)
	_ = pubsub.NewTopic(messages.TopicUrl)
	_ = pubsub.NewTopic(messages.TopicClients)
	_ = pubsub.NewTopic(messages.TopicJobs)

	if mylinks == nil {
		mylinks = make(chan messages.Post, 2)
		fmt.Printf("Type of a is %T", mylinks)
	}

	if reloadlinks == nil {
		reloadlinks = make(chan messages.ReloadPost, 2)
		fmt.Printf("Type of a is %T", reloadlinks)
	}

	go startLogging()

	dbConfig = persistence.DatabaseConfig{
		Filename: configData.Databasefile,
	}
	database = persistence.NewDatabase(dbConfig)

	logrus.Info("read database")

	database.ReadDatabase(dbConfig)
	//fixThumbNails(database)
	database.WriteDatabase(dbConfig)

	logrus.Info("read jobs")

	//Continue old jobs, if required
	jobsManager = jobs.NewJobs(configData.TargetFolder, configData.JobsFile)

	//xdownloads := downloads.Download{}
	xdownloads := downloads.New(configData, database, jobticker, jobsManager, mylinks, reloadlinks)

	go xdownloads.GetJobStatus()

	oldJobs := jobsManager.ReadJobs()
	for _, k := range oldJobs.Jobs {
		go xdownloads.HandleDownload(k.Link)
	}

	//messages = &UserInfo{}
	//messages.Messages = append(messages.Messages, &Message{USERINFO, "Welcome", 4})

	//handleDownload("https://www.ardmediathek.de/video/die-sendung-mit-dem-elefanten/5-dezember/wdr/Y3JpZDovL3dkci5kZS9CZWl0cmFnLTY2NTBhYzQwLTg3MTUtNDQwMS04YTA2LTM5YzJlZjNiZTYwZQ/")
	//youtube-dl https://www.ardmediathek.de/video/die-sendung-mit-dem-elefanten/5-dezember/wdr/Y3JpZDovL3dkci5kZS9CZWl0cmFnLTY2NTBhYzQwLTg3MTUtNDQwMS04YTA2LTM5YzJlZjNiZTYwZQ/ --write-thumbnail
	//opts := []string{"https://www.ardmediathek.de/video/die-sendung-mit-dem-elefanten/5-dezember/wdr/Y3JpZDovL3dkci5kZS9CZWl0cmFnLTY2NTBhYzQwLTg3MTUtNDQwMS04YTA2LTM5YzJlZjNiZTYwZQ/",
	//	"--write-thumbnail"}

	logrus.Info("Channels")

	logrus.Info("Handle links")
	for i := 0; i < 10; i++ {
		go xdownloads.HandleLink(i)
	}

	for i := 0; i < 2; i++ {
		go xdownloads.HandleReload(i)
	}

	logrus.Info("Router")
	r := mux.NewRouter()

	logrus.Info("Templates")
	templatesGlob := fmt.Sprintf("%s/*.html", configData.TemplatesFolder)
	templates = template.Must(template.ParseGlob((templatesGlob)))

	logrus.Info("Handlers")
	r.PathPrefix("/videos/").Handler(NewLoggingHandler("/videos/", configData.TargetFolder))
	r.PathPrefix("/thumbprints/").Handler(NewLoggingHandler("/thumbprints/", configData.TargetFolder))
	//r.PathPrefix("/thumbprints/").Handler(http.StripPrefix("/thumbprints/", http.FileServer(http.Dir(configData.TargetFolder))))

	staticGlob := fmt.Sprintf("%s/js/", configData.StaticsFolder)
	r.PathPrefix("/js/").Handler(http.StripPrefix("/js/", http.FileServer(http.Dir(staticGlob))))

	frontend := frontend.New(database)

	r.HandleFunc("/glabels", frontend.GetLabels)
	r.HandleFunc("/galbums", frontend.GetAlbums)
	r.HandleFunc("/glabelsforalbum", frontend.GetLabelForAlbum)
	r.HandleFunc("/gVideo", frontend.GetVideoData)
	r.HandleFunc("/gVideoByHash", frontend.GetVideoByHash)

	r.HandleFunc("/", handleIndex)
	r.HandleFunc("/play", spa)
	r.HandleFunc("/drop", handleDrops)
	r.HandleFunc("/joblist", handleJobList)
	r.HandleFunc("/queuelength", handleQueueLength)
	r.HandleFunc("/labels", handleLabels)

	r.HandleFunc("/albums", handleAlbums)
	r.HandleFunc("/setLabels", handleSetLabels)
	r.HandleFunc("/getLabels", handleGetLabels)
	r.HandleFunc("/getPieceInfo", handlePieceInfo)
	r.HandleFunc("/echo", echo)
	r.HandleFunc("/reload", handleReload)

	fmt.Println("Listen")
	serveOn := fmt.Sprintf("%s:%d", configData.Host, configData.Port)
	fmt.Printf("Serving On %s", serveOn)
	log.Fatal(http.ListenAndServe(serveOn, r))
	fmt.Println("Done")
}
