module muze.j7labs.de

go 1.16

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/websocket v1.4.2
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/text v0.20.0
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b

)
