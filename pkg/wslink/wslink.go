package wslink

import (
	"encoding/json"
	"fmt"
	"sync/atomic"
	"time"

	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
)

type State int

const (
	ACTIVE State = iota
	FAILED
)

type internalMessageType int

const (
	PING internalMessageType = iota
	PAYLOAD
)

type messageWrapper struct {
	InternalMessageType int
	SerializedMessage   string
}

var wsc *wsclientRegistry

var pingtimer = time.NewTicker(1 * time.Second)

type wsclientRegistry struct {
	wss []*WSClient
	id  int64
}

func (w *wsclientRegistry) cleanup() {
	for {
		nw := make([]*WSClient, 0)
		for _, w := range w.wss {
			if w.State == ACTIVE {
				nw = append(nw, w)
			} else {
				err := w.Connection.Close()
				if err != nil {
					logrus.Debug("Error closing ws" + fmt.Sprintf("%d %v", w.ID, err))
				}
				logrus.Debug("Dropped ws " + fmt.Sprintf("%d", w.ID))

			}

		}
		w.wss = nw
		time.Sleep(500 * time.Millisecond)
	}
}

func (w *wsclientRegistry) getNewID() (id int64) {
	atomic.AddInt64(&w.id, 1)

	id = w.id
	return
}

func (w *wsclientRegistry) addClient(ws *WSClient) {
	w.wss = append(w.wss, ws)
	logrus.Debug("Added ws " + fmt.Sprintf("%d", ws.ID))

}

type WSClient struct {
	Connection websocket.Conn
	Inchannel  *chan string
	Outchannel *chan string
	State      State
	ID         int64
}

func NewWSClient(conn websocket.Conn, oc *chan string, ic *chan string) (ws *WSClient) {
	if wsc == nil {
		wsc = &wsclientRegistry{
			wss: make([]*WSClient, 0),
			id:  0,
		}
		go wsc.cleanup()
	}

	ws = &WSClient{
		Connection: conn,
		Inchannel:  ic,
		Outchannel: oc,
		State:      ACTIVE,
		ID:         wsc.getNewID(),
	}

	go ws.writeToWS()
	go ws.ReadFromWS()
	time.Sleep(0)
	wsc.addClient(ws)

	return

}

func (ws *WSClient) ReadFromWS() {

	for {
		_, message, err := ws.Connection.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway, websocket.CloseAbnormalClosure) {
				//log.Printf("error: %v", err)
				logrus.Debug("Error for ws " + fmt.Sprintf("%d : %v", ws.ID, err))
				ws.State = FAILED
			}
			break
		}

		if ws.Inchannel != nil {
			select {
			case *ws.Inchannel <- string(message):

			default:
				fmt.Println("WS can't deliver message")
			}
		}

	}
}

func (ws *WSClient) writeToWS() {

	ping := messageWrapper{
		InternalMessageType: int(PING),
		SerializedMessage:   "{ msg: 'ping' }",
	}

	pingtext, err := json.Marshal(ping)
	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}

	for {

		select {
		case message, ok := <-*ws.Outchannel:
			if !ok {
				return
			}
			m := messageWrapper{
				InternalMessageType: int(PAYLOAD),
				SerializedMessage:   message,
			}

			text, err := json.Marshal(m)
			if err != nil {
				fmt.Printf("Error while Marshaling. %v", err)
			}

			err = ws.Connection.WriteMessage(websocket.TextMessage, []byte(text))
			if err != nil {
				ws.State = FAILED
				logrus.Debug("Error for ws " + fmt.Sprintf("%d : %v", ws.ID, err))

			}
		case <-pingtimer.C:
			err = ws.Connection.WriteMessage(websocket.TextMessage, []byte(pingtext))
			if err != nil {
				ws.State = FAILED
				logrus.Debug("Error for ws " + fmt.Sprintf("%d : %v", ws.ID, err))

			}
		}

		if ws.State == FAILED {
			return
		}

	}
}
