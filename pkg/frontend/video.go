package frontend

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func NotFoundHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	fmt.Fprint(w, "custom 404")
}

func (fe *Frontend) GetVideoData(w http.ResponseWriter, r *http.Request) {
	link := r.URL.Query().Get("link")

	piece, ok := fe.database.PiecesMap[link]
	// If the key exists
	if !ok {
		NotFoundHandler(w, r)
		return
	}

	json.NewEncoder(w).Encode(piece)

}

func (fe *Frontend) GetVideoByHash(w http.ResponseWriter, r *http.Request) {
	hash := r.URL.Query().Get("hash")

	for _, p := range fe.database.PiecesMap {
		if p.Hash == hash {
			json.NewEncoder(w).Encode(p)
			return

		}
	}

	NotFoundHandler(w, r)

}
