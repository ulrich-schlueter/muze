package frontend

import (
	"muze.j7labs.de/pkg/persistence"
)

type Frontend struct {
	database *persistence.Database
}

func New(database *persistence.Database) *Frontend {
	return &Frontend{
		database: database,
	}
}
