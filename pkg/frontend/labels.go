package frontend

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func (fe *Frontend) GetLabels(w http.ResponseWriter, r *http.Request) {

	labels := fe.database.AllLabels
	json.NewEncoder(w).Encode(labels)
}

func (fe *Frontend) GetLabelForAlbum(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Query().Get("name")

	labels, err := fe.database.GetSelectedLabelsForAlbum(name)
	if err != nil {
		fmt.Printf("Error retrieving album %v", err)
	}
	json.NewEncoder(w).Encode(labels)
}
