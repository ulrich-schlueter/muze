package frontend

import (
	"encoding/json"
	"net/http"
	"sort"

	"muze.j7labs.de/pkg/view"
)

func (fe *Frontend) GetAlbums(w http.ResponseWriter, r *http.Request) {

	label := r.URL.Query().Get("label")
	labeltype := r.URL.Query().Get("type")

	v := view.ViewData{}
	keys := make([]string, 0, len(fe.database.PiecesMap))

	for k := range fe.database.PiecesMap {
		keys = append(keys, k)
	}

	sort.SliceStable(keys, func(i, j int) bool {
		return fe.database.PiecesMap[keys[i]].TimeAdded.After(fe.database.PiecesMap[keys[j]].TimeAdded)
	})
	//sort.Strings(keys)
	//sort.Slice(timeSlice, func(i, j int) bool {
	//	return timeSlice[i].date.Before(timeSlice[j].date)
	//})

	if labeltype == "CONTROL" {
		switch label {
		case "NOLABELS":
			for _, k := range keys {
				p := fe.database.PiecesMap[k]
				if len(p.Labels) == 0 {
					v.Pieces = append(v.Pieces, view.FromPV(p))
				}
			}
		case "":
			for _, k := range keys {
				p := fe.database.PiecesMap[k]
				v.Pieces = append(v.Pieces, view.FromPV(p))
			}
		}

	} else {

		for _, k := range keys {
			p := fe.database.PiecesMap[k]
			for _, l := range p.Labels {
				if l == label || label == "" {
					v.Pieces = append(v.Pieces, view.FromPV(p))
					break
				}
			}
		}
	}

	json.NewEncoder(w).Encode(v.Pieces)

}
