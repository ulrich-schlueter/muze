package view

import (
	"strings"
	"time"

	"muze.j7labs.de/pkg/persistence"
)

type Piece struct {
	Name           string
	Title          string
	Link           string `json:"link"`
	Labels         []string
	ImportedLabels []string
	ThumbNail      string
	Duration       string
	TimeAdded      string
}

type ViewData struct {
	Pieces    []*Piece
	PiecesMap map[string]*Piece
	Labels    map[string]int
}

func FromPV(p *persistence.Piece) *Piece {
	dur := time.Duration(p.Duration) * time.Second
	piece := &Piece{
		Name:      p.Name,
		Title:     p.Title,
		Link:      p.Link,
		ThumbNail: p.ThumbNail,
		Duration:  dur.String(),
		TimeAdded: p.TimeAdded.Format("20060102150405"),
	}

	piece.ThumbNail = strings.ReplaceAll(piece.ThumbNail, "#", "%23")
	piece.Name = strings.ReplaceAll(piece.Name, "#", "%23")

	piece.Labels = make([]string, len(p.Labels))
	copy(piece.Labels, p.Labels)

	piece.ImportedLabels = make([]string, len(p.ImportedLabels))
	copy(piece.ImportedLabels, p.ImportedLabels)

	return piece
}
