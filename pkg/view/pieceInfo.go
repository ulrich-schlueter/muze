package view

import (
	"time"

	"muze.j7labs.de/pkg/persistence"
)

type PieceInfo struct {
	Title          string
	Name           string
	Link           string `json:"link"`
	Labels         []string
	ImportedLabels []string
	ThumbNail      string
	TimeAdded      time.Time
	Duration       string
}

func PieceInfoFromPV(p *persistence.Piece) *PieceInfo {
	dur := time.Duration(p.Duration) * time.Second
	pieceInfo := &PieceInfo{
		Name:      p.Name,
		Title:     p.Title,
		Link:      p.Link,
		ThumbNail: p.ThumbNail,
		TimeAdded: p.TimeAdded,
		Duration:  dur.String(),
	}

	copy(pieceInfo.Labels, p.Labels)
	copy(pieceInfo.ImportedLabels, p.ImportedLabels)

	return pieceInfo
}
