package persistence

import (
	"github.com/lithammer/shortuuid/v3"
)

func NewUUID() string {
	return shortuuid.New()
}
