package persistence

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gopkg.in/yaml.v3"
)

const DBCodeVersion = V1

type DBVersion string

const (
	V0 DBVersion = ""
	V1 DBVersion = "1"
)

type Piece struct {
	Title          string
	Name           string
	Link           string `json:"link"`
	Labels         []string
	ImportedLabels []string
	ThumbNail      string
	TimeAdded      time.Time
	Duration       float32
	Hash           string
}

func (p *Piece) DeepCopy() *Piece {
	piece := &Piece{
		Title:     p.Title,
		Name:      p.Name,
		Link:      p.Link,
		ThumbNail: p.ThumbNail,
		Duration:  p.Duration,
		TimeAdded: p.TimeAdded,
		Hash:      p.Hash,
	}

	piece.Labels = make([]string, len(p.Labels))
	copy(piece.Labels, p.Labels)

	piece.ImportedLabels = make([]string, len(p.ImportedLabels))
	copy(piece.ImportedLabels, p.ImportedLabels)

	return piece
}

type Database struct {
	Version           DBVersion
	VideoNames        []string          //V0
	Pieces            []*Piece          //V0
	PiecesMap         map[string]*Piece //V1
	Labels            map[string]int    //V0
	AllLabels         map[string]int    //V1
	AllImportedLabels []string          //V1
}

var (
	mu       sync.Mutex
	dbConfig DatabaseConfig
)

func NewDatabase(config DatabaseConfig) (d *Database) {
	dbConfig = config
	d = &Database{}
	return

}

func NewPiece(location string, thumbNail string, link string, labels []string, importedlabels []string, duration float32) *Piece {
	_, name := filepath.Split(location)
	_, tNail := filepath.Split(thumbNail)
	title := strings.TrimSuffix(name, filepath.Ext(name))
	hash := NewUUID()

	return &Piece{
		Title:          title,
		Name:           name,
		Link:           link,
		ThumbNail:      tNail,
		Labels:         labels,
		ImportedLabels: importedlabels, //V1
		TimeAdded:      time.Now(),
		Duration:       duration,
		Hash:           hash,
	}
}

func (d *Database) AppendPiece(p *Piece, dbConfig DatabaseConfig) {
	d.PiecesMap[p.Name] = p
	d.VideoNames = append(d.VideoNames, p.Name)
	d.WriteDatabase(dbConfig)
}

func (d *Database) CreateOrUpdatePiece(piece *Piece) {

	mu.Lock()
	defer mu.Unlock()

	d.PiecesMap[piece.Title] = piece

	d.AllLabels = d.GetLabels()
	d.WriteDatabase(dbConfig)
}

func (d *Database) GetLabels() (labels map[string]int) {
	labels = make(map[string]int)
	for _, p := range d.PiecesMap {
		for _, l := range p.Labels {
			if l != "" {
				value, ok := labels[l]
				if ok {
					labels[l] = value + 1
				} else {
					labels[l] = 1
				}
			}

		}

	}

	return
}

func (d *Database) SetLabels(name string, labels []string, dbconfig DatabaseConfig) (err error) {
	piece, ok := d.PiecesMap[name]
	if !ok {
		err = errors.New("can't find piece")
		return
	}

	piece.Labels = labels
	d.AllLabels = d.GetLabels()

	d.WriteDatabase(dbconfig)

	return
}

func (d *Database) GetSelectedLabelsForAlbum(album string) (labels map[string]bool, err error) {

	p, err := d.GetPiece(album)
	if err != nil {
		return
	}

	labels = make(map[string]bool)

	for l := range d.AllLabels {
		labels[l] = false
	}

	for _, l := range p.Labels {
		labels[l] = true
	}

	return
}

func (d *Database) GetPiece(name string) (*Piece, error) {
	piece, exists := d.PiecesMap[name]
	if exists {
		return piece, nil
	}

	err := errors.New("Piece not found")
	return nil, err
}

func (d *Database) GetPieceByLink(link string) (*Piece, error) {
	for _, p := range d.PiecesMap {
		if p.Link == link {
			return p, nil
		}
	}

	err := errors.New("Piece not found")
	return nil, err
}

func (d *Database) ReadDatabase(dConfig DatabaseConfig) {

	yfile, err := os.ReadFile(dConfig.Filename)
	if err != nil {
		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, d)
	if err2 != nil {

		log.Fatal(err2)
	}

	if d.Version != DBCodeVersion {
		log.Fatal(errors.New("incompatible database version, run admin step first"))
	}

	d.regenVideoNames()
	d.fixHashes()

}

func (d *Database) fixHashes() {

	for _, p := range d.PiecesMap {
		if p.Hash == "" {
			p.Hash = NewUUID()
		}
	}
}

func (d *Database) regenVideoNames() {
	videos := make([]string, 0)
	for _, p := range d.PiecesMap {
		videos = append(videos, p.Name)
	}
	d.VideoNames = videos

}

func (d *Database) GetDatabaseVersion() DBVersion {

	log.Println("reading " + dbConfig.Filename)
	yfile, err := os.ReadFile(dbConfig.Filename)
	if err != nil {
		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, d)
	if err2 != nil {

		log.Fatal(err2)
	}

	return d.Version

}

func (d *Database) ConvertDatabaseV0V1(dbConfig DatabaseConfig) {

	yfile, err := os.ReadFile(dbConfig.Filename)
	if err != nil {
		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, d)
	if err2 != nil {

		log.Fatal(err2)
	}

	if d.Version != V0 {
		log.Fatal(errors.New("version conflict, can't convert this database"))
	}

	d.AllLabels = d.GetLabels()

	if d.PiecesMap == nil {
		d.PiecesMap = make(map[string]*Piece)
	}
	for _, p := range d.Pieces {
		title := strings.TrimSuffix(p.Name, filepath.Ext(p.Name))
		p.Title = title
		d.PiecesMap[p.Title] = p
	}

	d.AllLabels = d.GetLabels()
	d.Pieces = nil
	d.Labels = nil
	d.Version = V1
}

func (d *Database) WriteDatabase(dbConfig DatabaseConfig) {
	yamlData, err := yaml.Marshal(&d)

	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}

	err = os.WriteFile(dbConfig.Filename, yamlData, 0644)
	if err != nil {
		fmt.Printf("Error writing DB. %v", err)
	}
}
