package config

import (
	"fmt"
	"log"
	"os"

	"gopkg.in/yaml.v3"
)

type Config struct {
	Databasefile string
	TargetFolder string
	Thumbnails   string
	NoThumbNail  string

	JobsFile        string
	TemplatesFolder string
	StaticsFolder   string
	InfoFolder      string
	LogFile         string
	Host            string
	Port            int
	YTDL            string
}

func Default() Config {
	return Config{
		Databasefile:    "database.yaml",
		TargetFolder:    "downloads/",
		Thumbnails:      "thumbnails/",
		NoThumbNail:     "NoThumbNail.png",
		JobsFile:        "jobs.yaml",
		TemplatesFolder: "templates",
		StaticsFolder:   "static/",
		InfoFolder:      "info/",
		LogFile:         "log.file",
		Host:            "0.0.0.0",
		Port:            8081,
		YTDL:            "yt-dlp",
	}
}

func (c *Config) WriteConfig(filepath string) error {
	yamlData, err := yaml.Marshal(&c)

	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
		return err
	}

	err = os.WriteFile(filepath, yamlData, 0644)
	if err != nil {
		fmt.Printf("Error writing config. %v", err)
		return err
	}
	return nil
}

func ReadConfig(filepath string) Config {

	c := Config{}
	yfile, err := os.ReadFile(filepath)
	if err != nil {
		log.Fatal(err)
	}

	err2 := yaml.Unmarshal(yfile, &c)
	if err2 != nil {

		log.Fatal(err2)
	}
	return c

}
