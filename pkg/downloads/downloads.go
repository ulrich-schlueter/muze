package downloads

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"muze.j7labs.de/pkg/config"
	"muze.j7labs.de/pkg/info"
	"muze.j7labs.de/pkg/jobs"
	"muze.j7labs.de/pkg/messages"
	"muze.j7labs.de/pkg/persistence"
	"muze.j7labs.de/pkg/pubsub"
)

type Download struct {
	config      config.Config
	database    *persistence.Database
	jobticker   *time.Ticker
	jobsManager *jobs.Jobs
	mylinks     chan messages.Post
	reloadlink  chan messages.ReloadPost
}

func New(config config.Config, database *persistence.Database, jobticker *time.Ticker, jobsManager *jobs.Jobs, mylinks chan messages.Post, reloadlink chan messages.ReloadPost) Download {
	return Download{
		config:      config,
		database:    database,
		jobticker:   jobticker,
		jobsManager: jobsManager,
		mylinks:     mylinks,
		reloadlink:  reloadlink,
	}
}

func (d *Download) runYTDLCommand(folder string, link string) (location string, thumbNail string, info string, err error) {

	thumbNail = ""
	location = ""
	//https://ostechnix.com/youtube-dl-tutorial-with-examples-for-beginners/
	ytdl := d.config.YTDL
	cmd := exec.Command(ytdl,
		"-o", folder+"/%(title)s.%(ext)s", "--write-thumbnail",
		"-f", "bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best",
		link)

	output, err := cmd.CombinedOutput()

	if err != nil {
		fmt.Println(err.Error())
		fmt.Print(string(output))
		var exerr *exec.ExitError
		if errors.As(err, &exerr) {
			fmt.Printf("%v %s\n", exerr.ProcessState, exerr.Stderr)
		}
		fmt.Print(string(output))

		return
	}

	//[download] Destination: Adventskalender - 5. Dezember-10091467.mp4
	//[download] Adventskalender - 5. Dezember-10091467.mp4 has already been downloaded
	//[ARDBetaMediathek] 10091467: Writing thumbnail to: Adventskalender - 5. Dezember-10091467.jpg
	// [Merger] Merging formats into "downloads/test4.mp4"
	lines := strings.Split(string(output), "\n")
	thumbNail = ""
	location = ""
	THUMBNAILTEXT := "Writing video thumbnail"
	DESTINATIONTEXT := "[download] Destination: "
	ALREADYDOWNLOADEDTEXT := "has already been downloaded"
	MERGERTEXT := "[Merger] Merging formats into "

	for _, l := range lines {
		t := strings.Index(l, THUMBNAILTEXT)
		loc := strings.Index(l, DESTINATIONTEXT)
		a := strings.Index(l, ALREADYDOWNLOADEDTEXT)
		m := strings.Index(l, MERGERTEXT)
		if t != -1 {
			i := strings.Index(l, ":")
			thumbNail = l[i+2:]
		}
		if loc != -1 {
			location = l[t+len(DESTINATIONTEXT):]
		}

		if a != -1 {
			location = l[len("[download] "):a]
			info = "Already downloaded"
		}
		if m != -1 {
			location = l[len(folder)+len(MERGERTEXT)+2 : len(l)-1]
			fmt.Print(location)
			info = "Already downloaded"
		}

	}
	location = strings.TrimSpace(location)
	thumbNail = strings.TrimSpace(thumbNail)
	fmt.Println("THUMBNAILTEXT:", thumbNail)
	fmt.Println("DESTINATIONTEXT:", location)
	//fmt.Print(string(output))
	return
}

func (d *Download) runYTDLJSON(link string) (title string, filename string, ext string, importedLabels []string, err error) {

	//ytdl := "youtube-dl"
	ytdl := d.config.YTDL
	// youtube-dl --dump-json https://www.youtube.com/watch?v=C7HatBbRSI0

	cmd := exec.Command(ytdl,
		"--dump-json", link,
	)
	output, err := cmd.Output()
	//output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(err.Error())
		fmt.Print(string(output))
		var exerr *exec.ExitError
		if errors.As(err, &exerr) {
			fmt.Printf("%v %s\n", exerr.ProcessState, exerr.Stderr)
		}

		return
	}
	tmpPath := fmt.Sprintf("%s/tmp-info.dump", d.config.InfoFolder)
	err = os.WriteFile(tmpPath, output, 0644)
	if err != nil {
		fmt.Printf("Error writing TmpFile. %v", err)
	}
	url, err := url.Parse(link)
	if err != nil {
		fmt.Printf("Error parsing url. %s %v", link, err)
		return
	}

	q := url.Query()
	targetID := q["v"][0]

	var result map[string]interface{}
	scanner := bufio.NewScanner(strings.NewReader(string(output)))
	buf := make([]byte, 0, 64*1024)
	scanner.Buffer(buf, 1024*1024)
	for scanner.Scan() {
		oneItem := scanner.Text()
		err = json.Unmarshal([]byte(oneItem), &result)
		if err != nil {
			fmt.Println(err.Error())
			return
		}
		if result["id"].(string) == targetID {
			break

		}
	}
	title = result["title"].(string)
	filename = result["filename"].(string)
	filenameStripped := strings.TrimSuffix(filename, filepath.Ext(filename))
	ext = result["ext"].(string)

	t := result["tags"].([]interface{})

	importedLabels = make([]string, len(t))
	for i, v := range t {
		importedLabels[i] = fmt.Sprint(v)
	}

	d.writeInfoString(d.config.InfoFolder, filenameStripped, string(output))

	return
}

func (d *Download) writeInfoString(folder string, name string, info string) (err error) {

	fullPath := fmt.Sprintf("%s/%s.%s", folder, name, "json")
	err = os.WriteFile(fullPath, []byte(info), 0644)
	if err != nil {
		fmt.Printf("Error writing Info. %v", err)
	}
	return
}

func (d *Download) GetJobStatus() {
	for {
		<-d.jobticker.C
		if d.jobsManager.UpdateJobsStats() {

			m := &messages.Message{MessageType: messages.JOBUPDATE, Message: "update", Elapses: 4}
			s, _ := json.Marshal(m)
			pubsub.PublishByTopic(messages.TopicClients, string(s))
		}

	}

}

func (d *Download) HandleDownload(link string) {

	targetFolder := d.config.TargetFolder

	m := &messages.Message{MessageType: messages.USERINFO, Message: "Downloading " + link, Elapses: 4}
	s, _ := json.Marshal(m)
	pubsub.PublishByTopic(messages.TopicClients, string(s))

	t, filename, e, importedLabels, err := d.runYTDLJSON(link)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	d.jobsManager.AddJob(&jobs.DownloadJob{
		Link:      link,
		Name:      t,
		Filename:  filename,
		Extention: e,
	})

	m = &messages.Message{MessageType: messages.JOBUPDATE, Message: "start", Elapses: 4}
	s, _ = json.Marshal(m)
	pubsub.PublishByTopic(messages.TopicClients, string(s))

	location, thumbNail, infoX, err := d.runYTDLCommand(targetFolder, link)
	if err != nil {
		fmt.Println("Failed to Download ", link, err.Error())

		m := &messages.Message{MessageType: messages.USERINFO, Message: "Downloaded failed", Elapses: 4}
		s, _ := json.Marshal(m)
		pubsub.PublishByTopic(messages.TopicClients, string(s))
		return
	}

	duration := float32(0.0)
	info, err := info.GetInfo(targetFolder + string(os.PathSeparator) + location)
	if err != nil {
		fmt.Println("Failed to determine run time ", link, err.Error())

	} else {
		duration = info.DurationInSeconds
	}

	piece := persistence.NewPiece(location, thumbNail, link, []string{}, importedLabels, duration)
	d.database.CreateOrUpdatePiece(piece)

	if infoX != "" {
		//	Messages = append(Messages, &Message{DOWNLOAD, info, 4})
		m := &messages.Message{MessageType: messages.JOBUPDATE, Message: "info", Elapses: 4}
		s, _ := json.Marshal(m)
		pubsub.PublishByTopic(messages.TopicJobs, string(s))
	}

	d.jobsManager.RemoveJob(link)
	m = &messages.Message{MessageType: messages.JOBUPDATE, Message: "end", Elapses: 4}
	s, _ = json.Marshal(m)
	pubsub.PublishByTopic(messages.TopicClients, string(s))

	m = &messages.Message{MessageType: messages.USERINFO, Message: "Finished Downloading " + link, Elapses: 4}
	s, _ = json.Marshal(m)
	pubsub.PublishByTopic(messages.TopicClients, string(s))

}

func (d *Download) HandleLink(id int) {

	for {
		message := <-d.mylinks
		fmt.Println(id, "Got", message.Url)
		d.HandleDownload(message.Url)
		fmt.Println("Done. Length now: ", len(d.mylinks))
	}
}

func (d *Download) HandleReload(id int) {

	targetFolder := d.config.TargetFolder
	for {
		message := <-d.reloadlink
		fmt.Printf("%d Got %s %s %s\n", id, message.Url, message.Name, message.Title)
		p, ok := d.database.PiecesMap[message.Title]
		if ok {

			//erase file
			path := targetFolder + string(os.PathSeparator) + p.Name
			if _, err := os.Stat(path); err == nil {
				// path/to/whatever exists
				fmt.Println("exists")
				os.Remove(path)
			} else if os.IsNotExist(err) {
				// path/to/whatever does *not* exist
				fmt.Println("does not exist")
			} else {
				// Schrodinger: file may or may not exist. See err for details.

				// Therefore, do *NOT* use !os.IsNotExist(err) to test for file existence
				fmt.Println(err.Error())
			}

		}

		d.HandleDownload(message.Url)
		fmt.Println("Done. Length now: ", len(d.reloadlink))
	}
}
