package pubsub

import (
	"errors"
)

type subscriber struct {
	channel *chan string
	queue   *BlockingQueue
}

type Topic struct {
	name         string
	subscribers  []*subscriber
	inputChannel chan string
}

var topics = make(map[string]*Topic)

func GetTopicNames() (names []string) {
	for k := range topics {
		names = append(names, k)
	}
	return
}

func GetTopicByName(name string) (topic *Topic, err error) {
	topic, ok := topics[name]
	if !ok {
		err = errors.New("topic not found")
	}
	return
}

func PublishByTopic(topicName string, message string) (err error) {
	t, err := GetTopicByName(topicName)
	if err != nil {

		return
	}
	t.Publish(message)
	return
}

func NewTopic(name string) (topic *Topic) {
	topic = &Topic{
		name:         name,
		subscribers:  make([]*subscriber, 0),
		inputChannel: make(chan string, 10),
	}
	topics[name] = topic

	return
}

func (s *subscriber) feedSubscriber() {
	for {
		msg := s.queue.Pop()
		*s.channel <- msg
	}
}

func (t *Topic) Subscribe() (callbackChannel *chan string) {
	cc := make(chan string, 1)
	s := &subscriber{channel: &cc,
		queue: NewBlockingQueue()}
	go s.feedSubscriber()
	t.subscribers = append(t.subscribers, s)
	callbackChannel = &cc
	return
}

func (t *Topic) Publish(message string) (err error) {

	for _, c := range t.subscribers {
		c.queue.Push(message)

	}
	return
}
