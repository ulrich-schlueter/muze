package pubsub

import (
	"sync"
)

type BlockingQueue struct {
	queue []string
	cond  *sync.Cond

	isEmpty bool
}

func NewBlockingQueue() (q *BlockingQueue) {

	return &BlockingQueue{
		cond:    sync.NewCond(&sync.Mutex{}),
		isEmpty: true}
}

func (q *BlockingQueue) Push(s string) {

	q.cond.L.Lock()
	q.queue = append(q.queue, s)
	q.isEmpty = false
	q.cond.L.Unlock()
	q.cond.Signal()

}

func (q *BlockingQueue) Pop() (s string) {
	q.cond.L.Lock()
	for q.isEmpty {
		q.cond.Wait()
	}

	s = q.queue[0]
	q.queue = q.queue[1:]
	if len(q.queue) == 0 {
		q.isEmpty = true
	}
	q.cond.L.Unlock()

	return

}
