package pubsub

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func helper(q *BlockingQueue, s string) {

	time.Sleep(1 * time.Second)
	q.Push(s)
}
func TestPushPop(t *testing.T) {
	q := NewBlockingQueue()
	q.Push("test")
	q.Push("test1")
	q.Push("test2")

	s := q.Pop()
	assert.Equal(t, "test", s, "Expecting to get the same value back")
	s = q.Pop()
	assert.Equal(t, "test1", s, "Expecting to get the same value back")
	s = q.Pop()
	assert.Equal(t, "test2", s, "Expecting to get the same value back")
}

func TestPopPush(t *testing.T) {
	q := NewBlockingQueue()
	go helper(q, "test")
	s := q.Pop()

	assert.Equal(t, "test", s, "Expecting to get the same value back")
}

func TestPushPopPop(t *testing.T) {
	q := NewBlockingQueue()
	q.Push("test2")
	s := q.Pop()
	assert.Equal(t, "test2", s, "Expecting to get the same value back")
	go helper(q, "test3")
	s = q.Pop()
	assert.Equal(t, "test3", s, "Expecting to get the same value back")

}
