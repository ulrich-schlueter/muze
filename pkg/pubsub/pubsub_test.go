package pubsub

import (
	"os"
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

var to *Topic
var testTopics = []string{"test1", "test2", "test3"}
var numberOfChannels = 5

func TestMain(m *testing.M) {
	to = NewTopic(testTopics[0])
	for i := 1; i < len(testTopics); i++ {
		_ = NewTopic(testTopics[i])
	}

	os.Exit(m.Run())
}

func TestPublish(t *testing.T) {

	var channels []*chan string

	for i := 0; i < numberOfChannels; i++ {

		c := to.Subscribe()
		channels = append(channels, c)
	}

	testMessage := "TestMessage"

	numberOfMessages := 501
	for i := 0; i < numberOfMessages; i++ {
		to.Publish(testMessage)
	}

	counter := 0
	noMoreMessage := false
	time.Sleep(1 * time.Second)
	for {
		for _, c := range channels {
			//n := fmt.Sprintf("%d", i)
			select {
			case msg := <-*c:
				//fmt.Println("received message on "+n, msg)
				if msg != "" {
					counter += 1
				}
			default:
				noMoreMessage = true
				//
			}
		}
		if noMoreMessage == true {
			break
		}
		//time.Sleep(time.Second / 100)
	}
	assert.Equal(t, numberOfChannels*numberOfMessages, counter, " Expecting to receive on message per channel")

}

func TestGetTopicNames(t *testing.T) {
	n := GetTopicNames()
	assert.Equal(t, len(n), len(testTopics), "Expecting one entry per test topic name")

	expectednames := testTopics
	assert.True(t, reflect.DeepEqual(expectednames, n), "list of topic names not correct")

}

func TestGetTopicByName(t *testing.T) {

	assert.True(t, len(testTopics) > 0, "expecting at least one test topics")

	n, err := GetTopicByName(testTopics[0])
	assert.NotNil(t, n, "expecting to get a topic")
	assert.Nil(t, err, "expecting err to be nil for successful topic retrieval")

	n, err = GetTopicByName(testTopics[0] + "-break")
	assert.NotNil(t, err, "expecting to get an error")
	assert.Nil(t, n, "expecting topic to be nil for failed topic retrieval")
}
