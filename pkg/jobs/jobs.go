package jobs

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"sync"

	"gopkg.in/yaml.v3"
)

type DownloadJob struct {
	Link            string
	Name            string
	Filename        string
	Extention       string
	Duration        int
	DownloadedBytes int64
}

type Jobs struct {
	mu           sync.Mutex
	Jobs         []*DownloadJob
	targetFolder string
	jobsFile     string
}

func NewJobs(targetFolder string, jobsFile string) (j *Jobs) {
	j = &Jobs{
		targetFolder: targetFolder,
		jobsFile:     jobsFile,
	}
	return
}

func (j *Jobs) UpdateJobsStats() (isIdle bool) {
	file := ""
	isIdle = false

	for _, n := range j.Jobs {
		isIdle = true

		files, _ := filepath.Glob(j.targetFolder + "/*.part")
		//fmt.Printf("Searching for %s found %d candidates\n", n.Name, len(files))

		if len(files) > 0 {
			file = ""

			for _, p := range files {

				searchFor := j.targetFolder + string(os.PathSeparator) + n.Name[0:15]
				ppiece := p[0:len(searchFor)]
				if ppiece == searchFor {
					//if strings.HasPrefix(p, j.targetFolder+string(os.PathSeparator)+n.Name) {
					file = p
					f, err := os.Open(p)

					if err != nil {
						fmt.Println(file, ":", err.Error())
						f.Close()
						continue
					}

					fi, err := f.Stat()
					if err != nil {
						fmt.Println(file, ":", err.Error())
						f.Close()
						continue
					}
					n.DownloadedBytes = fi.Size()

					break
				}

			}

		}
	}
	j.WriteJobs()
	return
}

func (j *Jobs) AddJob(job *DownloadJob) {
	j.mu.Lock()
	defer j.mu.Unlock()

	for _, n := range j.Jobs {
		if n.Link == job.Link {
			return
		}
	}

	j.Jobs = append(j.Jobs, job)
	j.WriteJobs()

}

func (j *Jobs) RemoveJob(link string) {
	j.mu.Lock()
	defer j.mu.Unlock()

	newJobs := Jobs{}

	for _, n := range j.Jobs {
		if n.Link != link {
			newJobs.Jobs = append(newJobs.Jobs, n)
		}
	}

	j.Jobs = newJobs.Jobs
	j.WriteJobs()

}

func (j *Jobs) ReadJobs() (jobs *Jobs) {

	yfile, err := ioutil.ReadFile(j.jobsFile)

	if err != nil {

		log.Fatal(err)
	}

	//data := make(map[interface{}]interface{})
	d := &Jobs{}
	err2 := yaml.Unmarshal(yfile, d)

	if err2 != nil {

		log.Fatal(err2)
	}

	jobs = d
	return
}

func (j *Jobs) WriteJobs() {
	yamlData, err := yaml.Marshal(&j)

	if err != nil {
		fmt.Printf("Error while Marshaling. %v", err)
	}
	//fmt.Println(string(yamlData))
	err = os.WriteFile(j.jobsFile, yamlData, 0644)
	if err != nil {
		fmt.Printf("Error writing Jobs. %v", err)
	}
}
