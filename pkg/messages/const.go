package messages

const (
	TopicLogging = "Logging"
	TopicUrl     = "Url"
	TopicClients = "Clients"
	TopicJobs    = "Jobs"
)
