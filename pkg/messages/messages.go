package messages

type MessageType int

type Message struct {
	MessageType MessageType
	Message     interface{}
	Elapses     int
}

type JobInfoMessage struct {
	Name   string
	Status string
}

const (
	USERINFO MessageType = iota
	DOWNLOAD
	JOBUPDATE
	QUEUELENGTH
	LABELS
	LABEL
	TITLE
)

type Post struct {
	Url string `json:"url"`
}

type ReloadPost struct {
	Url       string `json:"url"`
	Name      string `json:"name"`
	Title     string `json:"title"`
	Extension string `json:"extension"`
}

type PostList struct {
	List []*Post `json:"List"`
}
