package info

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"syscall"
	"time"
)

// /ffprobe -i "The Home Server I've Been Wanting.mp4" -v quiet -print_format json -show_format -show_streams -hide_banner

type FFMpegInfo struct {
	DurationInSeconds float32
	CreationTime      time.Time
}

func GetInfo(path string) (*FFMpegInfo, error) {
	return runFFProbe(path, "ffprobe")
}

func runFFProbe(path string, ffprobe string) (*FFMpegInfo, error) {

	cmd := exec.Command(ffprobe,
		"-i", path,
		"-v", "quiet",
		"-print_format", "json",
		"-show_format",
		"-show_streams",
		"-hide_banner",
	)
	output, err := cmd.Output()
	//output, err := cmd.CombinedOutput()
	if err != nil {
		fmt.Println(err.Error())

		var exerr *exec.ExitError
		if errors.As(err, &exerr) {
			fmt.Printf("%v %s\n", exerr.ProcessState, exerr.Stderr)
		}

		return nil, err
	}

	fmt.Print(string(output))
	var result map[string]interface{}

	err = json.Unmarshal([]byte(output), &result)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	f := result["format"].(map[string]interface{})
	val, ok := f["duration"]
	value := 0.0

	if ok {
		duration := val.(string)
		value, err = strconv.ParseFloat(duration, 32)
		if err != nil {

			value = 0
		}
	}

	info, err := os.Stat(path)
	if err != nil {
		fmt.Println(err.Error())
	}
	stat := info.Sys().(*syscall.Stat_t)
	ctime := time.Unix(int64(stat.Ctim.Sec), int64(stat.Ctim.Nsec))

	ff := &FFMpegInfo{
		DurationInSeconds: float32(value),
		CreationTime:      ctime,
	}
	return ff, nil
}
