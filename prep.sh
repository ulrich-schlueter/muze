pip3 install -U youtube-dl
pip3 install -U yt-dlp
mkdir -p downloads
mkdir -p info

if [ ! -f database.yaml ]; then
    echo "---" > database.yaml
fi

if [ ! -f jobs.yaml ]; then
    echo "---" > jobs.yaml
fi

