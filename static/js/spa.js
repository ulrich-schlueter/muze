
export { loadAlbums, loadModal, loadInfoModal, reload, urlposted, drop, playAll, saveLabels,play}

import { route } from "./modules/route.js";

var lastLabel = "";
var lastLabelType = "";
var ws;
var myModal = null;
var pieceInfoModal = null;
var playlist = []
var usermessages = []

setInterval(wsping, 1000);
setInterval(messages, 1000);
//

loadLabels()


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}


async function clearPostedDelayed(node) {

    await new Promise(resolve => setTimeout(resolve, 2000));
    node.value = ""
}


function urlposted(inputid) {
    if (inputid != "") {


        var x = document.getElementById(inputid);

        const params = {
            url: x.value,
        };
        const options = {
            method: 'POST',
            body: JSON.stringify(params)
        };


        fetch('/drop', options)
            .then(response => response.json())
            .then(response => {


                var p = document.getElementById('queuelength');

                p.innerText = response

                x.value = "posted"
                clearPostedDelayed(x)



            });

    }
}


function drop(ev) {
    var data = ev.dataTransfer.getData("text");
    const params = {
        url: data,
    };
    const options = {
        method: 'POST',
        body: JSON.stringify(params)
    };


    ev.preventDefault();

    var node = document.createElement('li');
    var ID = "links"
    node.textContent = data

    fetch('/drop', options)
        .then(response => response.json())
        .then(response => {


            var p = document.getElementById('queuelength');

            p.innerText = response

        });


}

function playAll(id) {
    var list = document.getElementById(id);
    playlisttext = list.dataset.album
    playlist = playlisttext.split('---')
    if (playlist.length > 0) {
        nextUp = playlist.shift()
        if (nextUp != '') {
            playSingleVideo(nextUp,"")
        }
    }
}

function playSingleVideo(link, title) {
    window.scrollTo(0, 0)

    var playinfo = document.getElementById('playinfo');
    var playlistNode = document.getElementById('playlist');
    var video = document.getElementById('player');

    playinfo.innerHTML = "<p>" + title + "</p>"

    if (playlist.length > 0) {

        var div = document.createElement('div');
        div.className = "list-group"
        var h3 = document.createElement('h4');
        h3.innerText = "Queue"
        playlistNode.innerHTML = ""
        playlistNode.appendChild(h3);
        playlistNode.appendChild(div);

        for (var i = 0; i < playlist.length; i++) {

            var item = document.createElement('button');
            item.classList = "list-group-item list-group-item-action"
            item.onclick = function(e) {
                l = e.target.innerText
                play(l)
            }
            div.appendChild(item);
            item.innerHTML = item.innerHTML + playlist[i];

        }

    }

    var source = document.getElementById('source');
    source.setAttribute('src', "/videos/" + link);
    if( title!="" ) 
    {
        video.setAttribute('title', title);
    }
    video.addEventListener('ended', played)
    video.load();
    video.play();
    

    
    route("play", "album" , link);
    //window.history.pushState({}, "","/play?album=" + link)

}

function play(link, title) {

    playlist = []
    playSingleVideo(link,title)
}



function played(link) {
    console.log("ended")
    if (playlist.length > 0) {
        nextUp = playlist.shift()
        if (nextUp != '') {
            playSingleVideo(nextUp,"")
        }
    }

}



function loadJobs() {
    fetch("/joblist")
        .then(res => res.json())
        .then(function(data) {
            if (data != null) {
                var div = document.getElementById('jobs');
                var ul = document.createElement('ul');
                var h3 = document.createElement('h4');

                h3.innerText = "Downloads"
                div.innerHTML = ""
                div.appendChild(h3);
                div.appendChild(ul);

                for (var i = 0; i < data.length; i++) {

                    var li = document.createElement('li');

                    ul.appendChild(li);
                    li.innerHTML = li.innerHTML + data[i].Link + " : " + data[i].DownloadedBytes;

                }
            } else {
                var div = document.getElementById('jobs');
                div.innerHTML = ""
            }

        })
}

function queuelength() {
    fetch("/queuelength")
        .then(res => res.json())
        .then(function(data) {
            var p = document.getElementById('queuelength');

            p.innerText = data


        })
}

function updateUserMessages() {

    var p = document.getElementById('messagearea');
    var d = document.getElementById('usermessages');

    var text = ""
    for (var i = 0; i < usermessages.length; i++) {
        text += usermessages[i].Message + " *** "
    }
    p.innerText = text
    if (text == "") {
        d.classList.remove("visible")
        d.classList.add('invisible')
    } else {
        d.classList.remove("invisible")
        d.classList.add('visible')
    }



}


function messages() {


    var x = []
    var changed = false
    for (var i = 0; i < usermessages.length; i++) {
        usermessages[i].Elapses = usermessages[i].Elapses - 1
        if (usermessages[i].Elapses > 0) {
            x.push(usermessages[i])
        } else {
            changed = true
        }
    }
    usermessages = x
    if (changed == true) {
        updateUserMessages()
    }
}

function reloadAlbums() {message
    loadAlbums(lastLabelType, lastLabel)
}


function wsping() {
    createWS();

    try {
        // Code to run
        [
            ws.send("PING")
        ]
    } catch (e) {
        // Code to run if an exception occurs
        [
            console.log(e)
        ]
    }
}

function loadAlbums(type, label) {

    lastLabel = label
    lastLabelType = type
    fetch("/albums?type=" + lastLabelType + "&label=" + lastLabel)
        .then(res => res.text())
        .then(function(data) {
            var d = document.getElementById('albums');

            d.innerHTML = data

        })
    window.history.pushState({},
        "Page 2", "/?label=" + lastLabel);

}

function loadLabels() {
    fetch("/labels")
        .then(res => res.text())
        .then(function(data) {
            var d = document.getElementById('labels');

            d.innerHTML = data

        })
}

function loadModal(album) {
    var albumesc = encodeURIComponent(album)

    fetch("/getLabels?name=" + albumesc)
        .then(res => res.text())
        .then(function(data) {
            var d = document.getElementById('labeleditor');

            d.innerHTML = data

            myModal = null
            myModal = new bootstrap.Modal(document.getElementById('labelModal'), {
                keyboard: false
            })
            myModal.show()
        })
}

function loadInfoModal(album) {
    var albumesc = encodeURIComponent(album)

    fetch("/getPieceInfo?name=" + albumesc)
        .then(res => res.text())
        .then(function(data) {
            var d = document.getElementById('pieceInfoSpace');

            d.innerHTML = data

            pieceInfoModal = null
            pieceInfoModal = new bootstrap.Modal(document.getElementById('infoModal'), {
                keyboard: false
            })
            pieceInfoModal.show()
        })
}

function reload(album) {
    var albumesc = encodeURIComponent(album)

    fetch("/reload?name=" + albumesc)
        .then(res => res.text())
        .then(function(data) {
           console.log(data)
        })
}


function saveLabels(name) {
    const checkboxes = document.querySelectorAll('input[name="labelselector"]:checked');
    let values = [];
    checkboxes.forEach((checkbox) => {
        var label = checkbox.labels[0].innerHTML
        if (label != "") {
            values.push(checkbox.labels[0].innerHTML);
        }
    });

    var textInput = document.getElementById('labeleditortext');
    const myArray = textInput.value.split(" ");
    myArray.forEach((word) => {
        if (word != "") {
            values.push(word);
        }
    })

    const params = {
        name: name,
    };
    const options = {
        method: 'POST',
        body: JSON.stringify(values)
    };
    var nameesc = encodeURIComponent(name)
    fetch('/setLabels?name=' + nameesc, options)
        .then(response => response.json())
        .then(response => {

            myModal.hide()
            myModal = null
            reloadAlbums()
        })

}

function onToggle(data) {
    alert(data);
}

function createWS() {
    if (ws) {
        return false;
    }
    ws = new WebSocket("ws://" + window.location.host + "/echo");
    ws.onopen = function(evt) {
        console.log("OPEN");
    }
    ws.onclose = function(evt) {
        console.log("CLOSE");
        ws = null;
    }
    ws.onmessage = function(evt) {
        var j = JSON.parse(evt.data)


        switch (j.InternalMessageType) {
            case 0: // Ping

                break;
            case 1: //custom
                var m = JSON.parse(j.SerializedMessage)
                switch (m.MessageType) {
                    // 0: USERINFO MessageType = iota
                    // 1: DOWNLOAD
                    // 2: JOBUPDATE
                    // 3: QUEUELENGTH
                    // 4: LABELS
                    // 4: LABEL (set label)
                    case 0:
                        usermessages.push(m)
                        updateUserMessages()
                        break
                    case 1:
                        break
                    case 2:
                        loadJobs()
                        break
                    case 1:
                        break
                    case 3:
                        break
                    case 4:
                        loadLabels()
                        break
                    case 5:
                        console.info(m)
                            //loadAlbums('CONTENT','{{ $key}}')"
                        break
                }

        }

    }
    ws.onerror = function(evt) {
        console.log("ERROR: " + evt.data);
    }
    return false;
}


window.loadAlbums=loadAlbums
window.play=play
window.loadModal=loadModal
window.loadInfoModal= loadInfoModal
window.reload= reload
window.urlposted= urlposted
window.drop= drop
window.playAll= playAll
window.saveLabels= saveLabels              