export { XWebSocket};


class XWebSocket extends EventTarget {

    constructor() {
        super()
        this.ws=null
        setInterval(this.wsping.bind(this), 1000);
    }


    wsping() {
        this.createWS(this);
        try {
            // Code to run
            [
                this.ws.send("PING")
            ]
        } catch (e) {
            // Code to run if an exception occurs
            [
                console.log(e)
            ]
        }
    }

    createWS(instance) {
        if (instance.ws) {
            return false;
        }
        instance.ws = new WebSocket("ws://" + window.location.host + "/echo");
        instance.ws.onopen = function(evt) {
            console.log("OPEN");
        }
        instance.ws.onclose = function(evt) {
            console.log("CLOSE");
            instance.ws = null;
        }
        instance.ws.onmessage = function(evt) {
            var j = JSON.parse(evt.data)


            switch (j.InternalMessageType) {
                case 0: // Ping

                    break;
                case 1: //custom
                    var m = JSON.parse(j.SerializedMessage)
                    console.info(m)
                    switch (m.MessageType) {
                        // 0: USERINFO MessageType = iota
                        // 1: DOWNLOAD
                        // 2: JOBUPDATE
                        // 3: QUEUELENGTH
                        // 4: LABELS
                        // 4: LABEL (set label)
                        case 0:
                            var cevent=new CustomEvent("usermessage", {       
                                detail: {                                    
                                    data: m,
                                },
                            })
                            instance.dispatchEvent(cevent);
                            break
                        case 1:
                            break
                        case 2:
                            var cevent=new CustomEvent("jobChanged", {       
                                detail: {                                    
                                    data: m,
                                },
                            })
                            instance.dispatchEvent(cevent);
                            break
                        case 1:
                            break
                        case 3:
                            break
                        case 4:
                            //loadLabels()
                            break
                        case 5:
                            console.info(m)
                                //loadAlbums('CONTENT','{{ $key}}')"
                            break
                    }

            }

        }
        instance.ws.onerror = function(evt) {
            console.log("ERROR: " + evt.data);
        }
        return false;
    }
}