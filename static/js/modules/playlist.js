export { PlayList};

var labeldiv
var labelObj
var template

var templateText=`
<div><h4> Playlist {{name}} </h4>
<ul id="style-5" class="timeline">
{{#each videos}}
    {{#ifLOREQ @index ../active }}
    <li active aria-current="true" class="full">{{ Title }}</li>
    {{ else}}
    <li [aria-current="false"]>{{ Title }}</li>
    {{/ifLOREQ}}
{{/each}}
<div>

</div>
</ul>
<button type="button" id="playlistback" class="btn btn-light">Back</button>
<button type="button" id="playlistnext" class="btn btn-light">Next</button>
</div>
`

class PlayList extends EventTarget {

    constructor(targetDiv , playerToWatch) {
        super();
        
        this.targetDiv=targetDiv
        this.targetObj = document.getElementById(targetDiv);
        this.playerToWatch=playerToWatch
        this.PlayList=null
        this.index=0
        this.template = Handlebars.compile(templateText)

        playerToWatch.addEventListener("played",  this.nextInPlayList.bind(this))
        
      }

    nextInPlayList(links) {
       
        this.next()
    }

    playIndex(index) {        
        var lenPlayList=this.PlayList.length;
        if (index>lenPlayList) {
            this.updateUI()
            return
        }
        var item=this.PlayList[index-1]
        this.updateUI()
        this.playerToWatch.play(item.Name,item.Title)


    }

    back() {
        this.index-=1        
        if (this.index<1) {
            this.index=1
        }
        this.playIndex(this.index)
    }

    next() {
        this.index+=1
        var lenPlayList=this.PlayList.length;
        if (this.index>lenPlayList) {
            this.index=1
        }
        this.playIndex(this.index)

    }

    updateUI() {
        var html = this.template({ videos: this.PlayList, name: event.detail.name, active: this.index-1 })
        this.targetObj.innerHTML = html
        var back=document.getElementById("playlistback")
        var next=document.getElementById("playlistnext")
        back.addEventListener("click",this.back.bind(this))
        next.addEventListener("click",this.next.bind(this))
    }


    setPlayList(event) {        
        console.log("setPlayList", event)
        this.PlayList=event.detail.list
        
       
        this.index=0
        this.next()

    }

}

