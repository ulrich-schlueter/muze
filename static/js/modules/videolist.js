export {
    VideoList
};

import * as route from "./route.js";



var templateText = `<span type="submit" id="videolistplayall" class="badge bg-info"  data-callback="playAll" data-label="{{ label }}" > Play all
</span>
<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    {{#each videos}} 
    <div class="col">
        <div class="card-body mh-100">

            <img src="/thumbprints/{{ ThumbNail }}" alt="{{ Name }}" width="274" height="154" data-callback="play" data-name="{{ Name }}" data-title="{{ Title }}" > </img>
            <p class="card-text"><span class="badge bg-warning">{{Duration}}</span>{{ Title }} {{#Labels}}<span class="badge bg-dark">{{this}}</span>{{/Labels}} </p>                              
            <span type="submit" class="badge bg-warning" data-callback="editlabels" data-name="{{ Name }}" data-title="{{ Title }}">
                ...
            </span>
            <span type="submit" class="badge bg-warning" onclick="loadInfoModal('{{ Title }}')">
                (i)
              </span>
              <span type="submit" class="badge bg-warning" onclick="reload('{{Title }}')">
                reload </span>           
        </div>
    </div>
    {{/each}}
</div>`


class VideoList extends EventTarget {

    constructor(targetDiv, labelsToWatch, labelEditor) {
        super();

        this.targetDiv = targetDiv
        this.labelsToWatch = labelsToWatch
        this.labelEditor = labelEditor
        this.lastLabelType=null
        this.lastLabelName=""
        this.videos=null

        this.targetObj = document.getElementById(targetDiv);
        this.template = Handlebars.compile(templateText)
        this.labelsToWatch.addEventListener("selected", this.labelSelected.bind(this))
        this.labelEditor.addEventListener("saved", this.labelsChanged.bind(this))
    }

    connect(instance) {
        //connect thumbnails
       
        var playAllButton = document.getElementById("videolistplayall");
        playAllButton.addEventListener("click", instance.playAll.bind(this))
        var list = instance.targetObj.getElementsByTagName("img");
        for (let i of list) {
            if (i.dataset.callback == "play") {
                //i.addEventListener("click",callerLabel.routeMe)
                i.addEventListener("click", instance.play.bind(this))
            }
        }

        list = instance.targetObj.getElementsByTagName("span");
        for (let i of list) {
            if (['editlabels'].includes(i.dataset.callback)) {
                //i.addEventListener("click",callerLabel.routeMe)
                i.addEventListener("click", instance.editLabelEvent.bind(this))
            }
        }
    }

    playAll(event) {

        var cevent = new CustomEvent("playAll", {
            detail: {
                list: this.videos,  
                name: event.currentTarget.dataset.label,           
            },
        })
        this.dispatchEvent(cevent);


    }

    play(event) {
        var cevent = new CustomEvent("play", {
            detail: {
                name: event.currentTarget.dataset.name,
                title: event.currentTarget.dataset.title
            },
        })
        this.dispatchEvent(cevent);
    }

   

    labelSelected(event) {
        var type = event.detail.xtype
        var param = event.detail.data
        this.lastLabelType=type
        this.lastLabelName=param

        var self = this
        route.route("","label",param)

        console.log(type, param, this.targetObj)

        fetch("/galbums?type=" + type + "&label=" + param)
            .then(res => res.text())
            .then(function (data) {
                var myobj = JSON.parse(data);
                self.videos=myobj
                var html = self.template({
                    videos: myobj,
                    label: param
                })
                self.targetObj.innerHTML = html
                self.connect(self)
            })
    }

    editLabelEvent(event) {
        var videoname = event.currentTarget.dataset.title
        this.labelEditor.show(videoname)
    }

    labelsChanged(event) {      
      var cevent=new CustomEvent("", {       
        detail: { 
            xtype: this.lastLabelType,
            data: this.lastLabelName
        },
    })
      this.labelSelected(cevent)
      this.labelsToWatch.loadLabels(this.labelsToWatch)
    }



}