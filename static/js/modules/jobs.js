export { Jobs };


var templateText = `
   <div  class="border h-50" id="jobs-drop" >
        ..drop here
    </div>
    <div class="form-outline">
        <input type="text" id="jobs-paste" class="form-control" placeholder="... or paste here" />
    </div>
    <div class="row">
      <h4>Downloads</h4>
        <div id="jobslist">
            <p></p>
        </div>
    </div>
  `



class Jobs extends EventTarget {

  constructor(targetDiv,websocket) {
    super();

    this.targetDiv = targetDiv
    this.targetObj = document.getElementById(targetDiv);
    this.websocket=websocket

    this.template = Handlebars.compile(templateText)
    var html = this.template({ })
    this.targetObj.innerHTML = html    
    this.dropTarget = document.getElementById("jobs-drop");
    this.dropTarget.addEventListener("drop",this)
    this.dropTarget.addEventListener("dragover",this)

    this.pasteTarget = document.getElementById("jobs-paste");
    this.pasteTarget.addEventListener("input",this)


    this.websocket.addEventListener("jobChanged",this)
  }
 
  handleEvent(event) {
    switch (event.type) {
      case "drop":     
        event.preventDefault();  
        var data = event.dataTransfer.getData("text");        
        this.dropData(data)
        break;
      case "dragover":          
          event.preventDefault();
          break;
      case "input":        
        var x = this.pasteTarget.value;        
        this.dropData(x)
        break;
       case "jobChanged":
          this.loadJobs(this)
          break;
    }
  }

  dropData(data) {
    const params = {
      url: data,
  };
    const options = {
      method: 'POST',
      body: JSON.stringify(params)
    };
    fetch('/drop', options)
    .then(response => response.json())
    .then(response => {

        var p = this.pasteTarget

        p.innerText = response
        this.clearPostedDelayed(this.pasteTarget)

    });

  }

  async  clearPostedDelayed(node) {

    await new Promise(resolve => setTimeout(resolve, 2000));
    node.value = ""
  }


   loadJobs(instance) {
    fetch("/joblist")
        .then(res => res.json())
        .then(function(data) {
            if (data != null) {
                var div = document.getElementById('jobslist');
                var ul = document.createElement('ul');
                var h3 = document.createElement('h4');

                
                div.innerHTML = ""
                div.appendChild(h3);
                div.appendChild(ul);

                for (var i = 0; i < data.length; i++) {

                    var li = document.createElement('li');

                    ul.appendChild(li);
                    li.innerHTML = li.innerHTML + data[i].Link + " : " + data[i].DownloadedBytes;

                }
            } else {
                var div = document.getElementById('jobslist');
                div.innerHTML = ""
            }

        })
}


}
