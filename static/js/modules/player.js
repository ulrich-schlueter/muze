export { Player};

import * as utils from "./utils.js";
import * as route from "./route.js";

const playInfo="playInfo"
const sourceElem="source"

class Player extends EventTarget {
    constructor(name = 0) {
        super();
        
        this.playerdiv=name
        
      }

      play(link, title) {
        window.scrollTo(0, 0)        
        var hash=this.getHash(title)
        
    
        var video = document.getElementById(this.playerdiv);
        var playinfo=document.getElementById(playInfo)        
        var source = utils.GetElementInsideContainer(this.playerdiv,sourceElem)    
    
        playinfo.innerHTML = "<p>" + title + "</p>"
        source.setAttribute('src', "/videos/" + link);
      
    
        if( title!="" ) 
        {
            video.setAttribute('title', title);
        }
    
        video.addEventListener('ended', this)
        video.load();        
        const promise = video.play();
        if(promise !== undefined){
            promise.then(() => {
                // Autoplay started
            }).catch(error => {
                // Autoplay was prevented.
                video.muted = true;
                video.play();
            });
        }
        }

    getHash(link) {
        var hash=""
        var linkesc = encodeURIComponent(link)
        fetch('/gVideo?link=' + linkesc)
            .then(res => res.text())    
            .then(function (data) {

                var myobj = JSON.parse(data);                               
                hash=myobj.Hash
                route.route("","link",hash)
            })

            
    }

    handleEvent(event) {
        switch (event.type) {
            case "ended":
                this.played(event)
              break;
            case "dblclick":
              // some code here…
              break;
          }
    }

    played(event) {
        console.log("ended",event)           
        
        var cevent=new CustomEvent("played", {       
            detail: { event },
        })
        
        this.dispatchEvent(cevent);
    
    }
}    

