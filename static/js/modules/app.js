export { App }


import * as player from "./player.js";
import * as labels from "./labels.js";
import * as videolist from "./videolist.js";
import * as playlist from "./playlist.js";
import * as jobs from "./jobs.js";
import * as xwebSocket from "./websocket.js"
import * as usermessages from "./usermessages.js"
import * as labelEditor from "./labeleditor.js"

class App extends EventTarget {

    constructor(targetDiv , main) {
        super();
        
        this.init()
      }

    init() {
        Handlebars.registerHelper('ifEQ', function(v1, v2, options) {
            if(v1 === v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          });

        Handlebars.registerHelper('ifLOREQ', function(v1, v2, options) {
            if(v1 <= v2) {
              return options.fn(this);
            }
            return options.inverse(this);
          });


        this.webSocket=new xwebSocket.XWebSocket()
        this.xplayer=new player.Player("playercontrol")
        this.xPlayList=new playlist.PlayList("playlist",this.xplayer)
        this.xLabels=new labels.Label("labels")
        
        this.labelEditor=new labelEditor.LabelEditor("labeleditor")
        this.xVideolist=new videolist.VideoList("albums",this.xLabels,this.labelEditor)
        this.xVideolist.addEventListener("play",this)
        this.xVideolist.addEventListener("playAll",this)
        this.jobs=new jobs.Jobs("jobs",this.webSocket)     
        this.usermessages=new usermessages.UserMessages("usermessages",this.webSocket)
        
        //this.xplayer.play("test video.mp4","test")
        this.setState()
    }

    setState() {
        const urlParams = new URLSearchParams(window.location.search);              
        var hash=urlParams.get('link');
        if (hash != "") {
            this.setLink(hash)
        }
        var label=urlParams.get('label');
        if (label != null) {
            var cevent=new CustomEvent("selected", {       
                detail: { 
                    xtype: "CONTENT", 
                    data: label
                },
            })
            this.xVideolist.labelSelected(cevent)
        }
       
    }

    setLink(hash) {
        var self=this
        var hashesc = encodeURIComponent(hash)
        fetch('/gVideoByHash?hash=' + hashesc)
            .then(response => response.json())
            .then(function (data) {
                                                                
                self.xplayer.play(data.Name,data.Title)
            })
        
            
    }

    handleEvent(event) {
        switch (event.type) {
            case "play":
                this.xplayer.play(event.detail.name,event.detail.title)
                break;            
            case "playAll":
                this.xPlayList.setPlayList(event)            
          }
    }

    route(a,c,b) {
        switch (a) {
            case labels.tag: 
                this.xVideolist.setLabel(c)
                break;
            case 1: //custom

        }
    }

}

