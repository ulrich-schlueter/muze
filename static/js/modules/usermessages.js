export { UserMessages};

import * as utils from "./utils.js";

const playInfo="playInfo"
const sourceElem="source"

class UserMessages extends EventTarget {
    constructor(name = 0, websocket) {
        super();
        
        this.targetdiv=name;
        this.websocket=websocket;
        this.target = document.getElementById(this.targetdiv);
        this.websocket.addEventListener("usermessage",this);
        this.usermessages=[]

        setInterval(this.cleanOldMessages.bind(this), 1000);
        
        
      }    

    handleEvent(event) {
        switch (event.type) {
            case "usermessage":
                var m=event.detail.data
                this.usermessages.push(m)
                this.updateUserMessages()
                console.log(event)
            case "jobChanged":
                    console.log(event)
            break;            
          }
    }



    updateUserMessages() {
        
        var d = this.target
        var p=utils.GetElementInsideContainer(this.targetdiv,'messagearea')        
    
        var text = ""
        for (var i = 0; i < this.usermessages.length; i++) {
            text += this.usermessages[i].Message + " *** "
        }
        p.innerText = text
        if (text == "") {
            d.classList.remove("visible")
            d.classList.add('invisible')
        } else {
            d.classList.remove("invisible")
            d.classList.add('visible')
        }
    
    
    
    }

    cleanOldMessages() {

        var x = []
        var changed = false
        for (var i = 0; i < this.usermessages.length; i++) {
            this.usermessages[i].Elapses = this.usermessages[i].Elapses - 1
            if (this.usermessages[i].Elapses > 0) {
                x.push(this.usermessages[i])
            } else {
                changed = true
            }
        }
        this.usermessages = x
        if (changed == true) {
            this.updateUserMessages()
        }
    }
    
}    

