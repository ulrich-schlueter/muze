export { LabelEditor };

var tag = "LABEL"

var templateText= `<!-- Modal -->
<div class="modal fade" id="labelModal" tabindex="-1" aria-labelledby="labelModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="labelModal">Select Labels</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>    
            <div class="modal-body">
                <div id="labels" class="container">
                    <p class="border">                                                
                         {{#with info}}  {{#each labels}} 
                        <input name="labelselector" type="checkbox" class="btn-check" id="btn-check-outlined-{{@key}}" autocomplete="off" {{#if this}} checked {{/if}}>
                        <label class="btn btn-outline-warning" for="btn-check-outlined-{{@key}}">{{@key}}</label>                                                         
                        {{/each}}  {{/with}}                                                       
                    </p>
                    <input id="labeleditortext" type="text" class="form-control" placeholder="" aria-label="newlabel" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" data-callback="save" data-name="{{info.name}}">Save changes</button>
            </div>        
        </div>
    </div>
</div>
`

var templateText2 = `  {{log "debug logging" level="debug"}}
                        {{#with info}}  {{#each labels}}&{{this}} {{@key}}{{/each}}
                       
                        {{/with}}
                        Name:{{info.name}}
`

class LabelEditor extends EventTarget {

    constructor(targetDiv) {
        super();

        this.targetDiv = targetDiv
        this.labelObj = document.getElementById(targetDiv);
        this.template = Handlebars.compile(templateText);
        this.myModal= null
      
    }

    
    savelabels(event) {
        var name=event.currentTarget.dataset.name
        var self=this
        console.log(name,event)
        const checkboxes = document.querySelectorAll('input[name="labelselector"]:checked');
        let values = [];
        checkboxes.forEach((checkbox) => {
            var label = checkbox.labels[0].innerHTML
            if (label != "") {
                values.push(checkbox.labels[0].innerHTML);
            }
        });
    
        var textInput = document.getElementById('labeleditortext');
        const myArray = textInput.value.split(" ");
        myArray.forEach((word) => {
            if (word != "") {
                values.push(word);
            }
        })
    
        const params = {
            name: name,
        };
        const options = {
            method: 'POST',
            body: JSON.stringify(values)
        };
        var nameesc = encodeURIComponent(name)
        fetch('/setLabels?name=' + nameesc, options)
            .then(response => response.json())
            .then(response => {
    
                self.myModal.hide()
                self.myModal= null


                var cevent=new CustomEvent("saved", {       
                    detail: { 
                        xtype: 'CONTENT', 
                        data: name
                    },
                })
                
                this.dispatchEvent(cevent);
            })
    
    }
  

    show(video) {
        console.log("labelEditorShow",video)
        this.loadLabels(video)
    }



     loadLabels(name){
        var albumesc = encodeURIComponent(name)
        var self=this

        fetch("/glabelsforalbum?name=" + albumesc)
            .then(res => res.text())
            .then(function(data) {
                // /var d = document.getElementById('labeleditor');

                var myobj = JSON.parse(data);
                var editorData={
                    labels: myobj,
                    name: name,
                }
                var html = self.template({ info: editorData })
                self.labelObj.innerHTML=html

                var list = self.labelObj.getElementsByTagName("button");
                for (let i of list) {
                    if (i.dataset.callback == "save") {                    
                    i.addEventListener("click", self.savelabels.bind(self))
                    }
                }

                
                self.myModal = new bootstrap.Modal(document.getElementById('labelModal'), {
                    keyboard: false
               })
               self.myModal.show()
            })
     }
   
}



