export { Label, tag };

var tag = "LABEL"


var templateText = `
<p class="border">
    <span class="badge bg-warning" data-callback="routeMe" data-type="CONTROL" data-param="">
        (ALL)
    </span>
    <span class="badge bg-warning" data-callback="routeMe" data-type="CONTROL" data-param="NOLABELS">
        (No Labels)       
    </span> {{#each labels}}
    <span class="badge bg-warning"  data-callback="routeMe" data-type="CONTENT" data-param="{{@key}}">
        #{{@key}}: {{this}}
    </span> {{/each}}
</p>
`

class Label extends EventTarget {

    constructor(targetDiv) {
        super();

        this.targetDiv = targetDiv
        this.labelObj = document.getElementById(targetDiv);
        this.template = Handlebars.compile(templateText)
        this.loadLabels(this)
    }

    loadLabels(callerLabel) {
        fetch("/glabels")
            .then(res => res.text())
            .then(function (data) {

                var myobj = JSON.parse(data);
                var html = callerLabel.template({ labels: myobj })
                callerLabel.labelObj.innerHTML = html
                
                var list=callerLabel.labelObj.getElementsByTagName("span")
                for (let i of list) {
                    if (i.dataset.callback=="routeMe") {
                        //i.addEventListener("click",callerLabel.routeMe)
                        i.addEventListener("click",callerLabel)
                    }
                }
            })
    }

    handleEvent(event) {
        switch (event.type) {
            case "click":
                this.raiseLabelSelectedEvent(event)
              break;            
          }
    }
  

    raiseLabelSelectedEvent(event) {
        var data=event.currentTarget.dataset
        
        var cevent=new CustomEvent("selected", {       
            detail: { 
                xtype: data.type, 
                data: data.param 
            },
        })
        
        this.dispatchEvent(cevent);
    }
}



