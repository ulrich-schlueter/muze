package main

import (
	"fmt"
	"os"

	"golang.org/x/text/encoding/charmap"
)

func main() {
	ss := "SYMPHONIC METAL & ROCK 🔥 Piano🎹 Violin🎻 Guitar🎸blend Boost energy while Working / Gaming 3-Hr"
	dec, err := charmap.ISO8859_1.NewDecoder().String(ss)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(os.Stdout, dec)

}
