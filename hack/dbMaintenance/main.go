package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"

	"muze.j7labs.de/pkg/config"
	"muze.j7labs.de/pkg/persistence"
)

func main() {
	var xconfig = config.Default()
	dbConfig := persistence.DatabaseConfig{
		Filename: xconfig.Databasefile,
	}
	database := persistence.NewDatabase(dbConfig)

	dbVersion := database.GetDatabaseVersion()
	if dbVersion == persistence.V0 {
		database.ConvertDatabaseV0V1(dbConfig)
		//RetrieveLabelsFromInfoFiles(xconfig.InfoFolder, *database)
		database.WriteDatabase(dbConfig)
		return
	}

	fmt.Println("Nothing to do")

}

func RetrieveLabelsFromInfoFiles(path string, d persistence.Database) {
	iterate(path, d)
}

func iterate(path string, d persistence.Database) {
	filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Fatalf(err.Error())
		}
		if info.IsDir() {
			return nil
		}
		jsonBArray, err := os.ReadFile(path)
		if err != nil {
			log.Fatalf(err.Error())
		}

		var result map[string]interface{}
		json.Unmarshal([]byte(jsonBArray), &result)

		name := info.Name()
		title := strings.TrimSuffix(name, filepath.Ext(name))

		fmt.Printf("File Name: %s\n", name)
		piece, exists := d.PiecesMap[title]
		if exists {
			tags, exists := result["tags"].([]interface{})

			if exists {
				for _, t := range tags {
					fmt.Printf("   %s\n", t)
					piece.ImportedLabels = append(piece.ImportedLabels, t.(string))
				}
			}
		}
		return nil

	})
}
