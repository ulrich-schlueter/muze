package main

import (
	"flag"
	"fmt"
	"log"

	"muze.j7labs.de/pkg/info"
)

func main() {

	var path string
	flag.StringVar(&path, "path", "", "filepath")

	flag.Parse()

	fmt.Println("file", path)

	fm, err := info.GetInfo(path)
	if err != nil {
		log.Fatalf(err.Error())
	}

	fmt.Printf("%+v", fm)
}
