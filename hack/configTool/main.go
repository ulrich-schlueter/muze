package main

import (
	"fmt"

	"github.com/davecgh/go-spew/spew"

	"muze.j7labs.de/pkg/config"
)

func main() {
	//var config = config.Default()
	//config.WriteConfig("./defaultConfig.yaml")

	c := config.ReadConfig("./defaultConfig.yaml")
	fmt.Printf("%+v", c)
	spew.Dump(c)
}
