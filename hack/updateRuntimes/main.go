package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/davecgh/go-spew/spew"
	"muze.j7labs.de/pkg/config"
	"muze.j7labs.de/pkg/info"
	"muze.j7labs.de/pkg/persistence"
)

func main() {

	var configData = config.Default()

	configpath := flag.String("configfile", "", "the config file")
	flag.Parse()

	if *configpath != "" {
		configData = config.ReadConfig(*configpath)
	}
	spew.Dump(configData)

	dbConfig := persistence.DatabaseConfig{
		Filename: configData.Databasefile,
	}
	database := persistence.NewDatabase(dbConfig)
	database.ReadDatabase(dbConfig)

	for _, piece := range database.PiecesMap {
		if piece.Duration == 0 {
			path := configData.TargetFolder + string(os.PathSeparator) + piece.Name
			fm, err := info.GetInfo(path)
			if err != nil {
				log.Println(err.Error())
			} else {
				piece.Duration = fm.DurationInSeconds
				if piece.TimeAdded.IsZero() {
					piece.TimeAdded = fm.CreationTime
				}

			}

			fmt.Printf("%+v", fm)
		}
	}

	database.WriteDatabase(dbConfig)

}
